﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Enums
{
    public enum ApplicationStatus
    {
        Accepted,
        Rejected
    }
}
