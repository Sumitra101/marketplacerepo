﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Qkly.Marketplace.Domain.Enums
{
    public enum Fluency
    {
        [Description("Native/Bilingual")]
        NativeBilingual,
        Basic,
        [Description("Read/Write")]
        ReadWrite,
        Conversational

    }
}
