﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileEducation
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } //foreign key
        public Profile Profile { get; set; }
        public Guid EducationId { get; set; } //foreign key
        public string AreaOfStudy { get; set; }
        public string School { get; set; }
        public DateTime StartYear { get; set; }
        public DateTime EndYear { get; set; }
        public bool IsCurrentlyEnrolled { get; set; }

    }
}
