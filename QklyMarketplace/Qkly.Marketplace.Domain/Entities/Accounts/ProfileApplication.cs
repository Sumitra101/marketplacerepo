﻿using Qkly.Marketplace.Domain.Entities.IndustryManagement;
using Qkly.Marketplace.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileApplication
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }
        public Profile Profile { get; set; }
        public Guid IndustryId { get; set; }
        public Industry Industry { get; set; }
        public Guid ServiceId { get; set; }
        public Service Service { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
        public DateTime ApplicationDate { get; set; }
    }
}
