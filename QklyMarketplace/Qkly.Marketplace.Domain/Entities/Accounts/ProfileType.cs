﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
