﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileFirm
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } //foreign key
        public Profile Profile { get; set; }
        public Guid FirmId { get; set; }  //foreign key
        public Firm Firm { get; set; }
        public Guid MemberTypeId { get; set; } //foreign key
    }
}
