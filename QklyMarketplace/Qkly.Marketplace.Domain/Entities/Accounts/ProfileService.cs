﻿using Qkly.Marketplace.Domain.Entities.IndustryManagement;
using Qkly.Marketplace.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileService
    {
        public Guid ProfileId { get; set; }
        public Guid ServiceId { get; set; }
        public Profile Profile { get; set; }
        public Service Service { get; set; }
        public decimal BillRate { get; set; }
        public ProfileServiceStatus Status { get; set; }
    }
}
