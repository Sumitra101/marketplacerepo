﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileResume
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } //foreign key
        public Profile Profile { get; set; }
        public string ResumeUrl { get; set; }
    }
}
