﻿using Qkly.Marketplace.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileLanguage
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }  //foreign key
        public Profile Profile { get; set; }
        public Guid LanguageId { get; set; } //foreign key
        public Fluency Fluency { get; set; }
    }
}
