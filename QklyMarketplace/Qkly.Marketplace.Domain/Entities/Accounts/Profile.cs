﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class Profile
    {
        public Guid Id { get; set; }
        public Guid ServicePlanId { get; set; }  //foreign key
        public Guid ProfileTypeId { get; set; } //foreign key
        public ProfileType ProfileType { get; set; }
        public Guid LocationId { get; set; } //foreign key
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Tagline { get; set; }  //ambiguous
        public string Description { get; set; }
        public bool IsSeller { get; set; }
        public bool HasActiveSubscription { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Experience { get; set; }
        public string Headline { get; set; }
        public string ProfessionalBio { get; set; }
        public string ProfilePictureUrl { get; set; }
        public bool IsVetted { get; set; }
        public string GoogleMapLocationTag { get; set; }
        public string Availability { get; set; }

    }
}
