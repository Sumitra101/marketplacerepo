﻿using Qkly.Marketplace.Domain.Entities.IndustryManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileSkill
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }  //foreign key
        public Profile Profile { get; set; }
        public Guid SkillId { get; set; }  //foreign key
        public Skill Skill { get; set; }
        public Guid SkillLevelId { get; set; } //foreign key
        public SkillLevel SkillLevel { get; set; }
    }
}
