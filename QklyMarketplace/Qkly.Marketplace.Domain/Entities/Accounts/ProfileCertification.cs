﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Accounts
{
    public class ProfileCertification
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }
        public Guid CertificationId { get; set; }
        public int Number { get; set; }
        public DateTime Year { get; set; }

    }
}
