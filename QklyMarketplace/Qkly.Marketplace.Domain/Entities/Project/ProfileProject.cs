﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class ProfileProject
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; } // foreign key
        public Project Project { get; set; }
        public Guid ProfileId { get; set; } // foreign key
        public Profile Profile { get; set; }
    }
}