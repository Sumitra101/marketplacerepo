﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CreatedById { get; set; } // foreign key

        public Profile CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public bool IsDeleted { get; set; }
        public decimal CompletePercentage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid ModifiedById { get; set; } // foreign key

        public Profile ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string Description { get; set; }
        public Guid StatusId { get; set; } // foreign key

        public Status Status { get; set; }
        public bool IsWeighted { get; set; }
    }
}