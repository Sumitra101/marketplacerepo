﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class Timeentry
    {
        public Guid Id { get; set; }
        public Guid EngagementId { get; set; } // foreign key
        public Engagement Engagement { get; set; }
        public Guid JobId { get; set; } // foreign key

        //public Job Job { get; set; }
        public Guid CreatedById { get; set; } // foreign key

        public Profile CreatedBy { get; set; }
        public Guid LastModifiedById { get; set; } // foreign key

        public Profile ModifiedBy { get; set; }
        public decimal TotalHours { get; set; }

        public decimal Hours { get; set; }
        public decimal Minutes { get; set; }
        public decimal Seconds { get; set; }
        public decimal TeStartTime { get; set; }
        public decimal TeStopTime { get; set; }
        public bool IsStopped { get; set; }
        public bool IsPaused { get; set; }
        public bool IsSubmitted { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsManual { get; set; }
        public bool IsModified { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}