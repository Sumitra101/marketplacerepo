﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class Engagement
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } // foreign key
        public Profile Profile { get; set; }
        public Guid JobId { get; set; } // foreign key

        //public Job Job { get; set; }
        public decimal HourlyCostRate { get; set; }

        public decimal HourlyBillRate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}