﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.IndustryManagement
{
    public class Industry
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public string FilePath { get; set; }
        public bool IsActive { get; set; }
    }
}
