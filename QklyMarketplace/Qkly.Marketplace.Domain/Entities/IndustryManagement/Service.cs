﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.IndustryManagement
{
    public class Service
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal MinimumBillRate { get; set; }
        public string FilePath { get; set; }
        public bool IsActive { get; set; }
    }
}
