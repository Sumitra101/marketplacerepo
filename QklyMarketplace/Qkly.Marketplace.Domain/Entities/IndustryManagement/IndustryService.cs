﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.IndustryManagement
{
    public class IndustryService
    {
        public Guid IndustryId { get; set; }
        public Guid ServiceId { get; set; }
        public Industry Industry { get; set; }
        public Service Service { get; set; }
    }
}
