﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.IndustryManagement
{
    public class SkillLevel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
