﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.IndustryManagement
{
    public class ServiceJobType
    {
        public Guid ServiceId { get; set; }
        public Guid JobTypeId { get; set; }
        public Service Service { get; set; }
        public JobType JobType { get; set; }
    }
}
