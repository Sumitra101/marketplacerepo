﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Job
{
    public class ProfileJobOffer
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } // foreign key
        public Profile Profile { get; set; }
        public Guid JobId { get; set; } // foreign key

        //public Job Job { get; set; }
        public Guid OfferedById { get; set; } // foreign key

        public Profile OfferedBy { get; set; }
        public DateTime OfferredDate { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsAccepted { get; set; }
        public string Message { get; set; }
        public bool IsFavorite { get; set; }
    }
}