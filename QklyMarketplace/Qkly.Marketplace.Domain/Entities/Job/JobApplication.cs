﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class JobApplication
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; } // foreign key
        public Profile Profile { get; set; }
        public Guid JobId { get; set; } // foreign key

        //public Job Job { get; set; }
        public Guid StatusId { get; set; } // foreign key

        public Status Status { get; set; }
        public DateTime ApplicationDate { get; set; }

        public DateTime UpdatedDate { get; set; }
        public bool IsFavorite { get; set; }
    }
}