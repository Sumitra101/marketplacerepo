﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Invoice
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid BuyerId { get; set; } // foreign key
        public Profile Buyer { get; set; }

        public Guid EngagementId { get; set; } // foreign key
        public Engagement Engagement { get; set; }
        public bool PaymentStatus { get; set; }
        public int InvoiceNumber { get; set; }
        public int InvoiceTotal { get; set; }
        public decimal TotalHour { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime InvoiceDueDate { get; set; }
        public string FilePath { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}