﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Invoice
{
    public class InvoiceLineItem
    {
        public Guid Id { get; set; }
        public Guid InvoiceId { get; set; } // foreign key
        public Invoice Invoice { get; set; }
        public DateTime TeDate { get; set; }
        public decimal TotalHours { get; set; }
        public decimal Rate { get; set; }
        public int LineItemTotal { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}