﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Invoice
{
    public class InvoiceLineItemDetail
    {
        public Guid Id { get; set; }
        public Guid LineItemId { get; set; } // foreign key
        public InvoiceLineItem LineItem { get; set; }
        public Guid TimeEntryId { get; set; } // foreign key
        public Timeentry Timeentry { get; set; }
    }
}