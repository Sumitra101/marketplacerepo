﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Invoice
{
    public class Payment
    {
        public Guid Id { get; set; }
        public Guid InvoiceId { get; set; } // foreign key
        public Invoice Invoice { get; set; }
        public Guid ReceivedById { get; set; } // foreign key
        public Profile ReceivedBy { get; set; }
        public Guid PaidById { get; set; } // foreign key
        public Profile PaidBy { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string ReceiptUrl { get; set; }
        public string Description { get; set; }
        public Guid StatusId { get; set; } // foreign key
        public Status Status { get; set; }
    }
}