﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Plans
{
    public class PlanFeature
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
        public bool IsRequired { get; set; }
        public string DataType { get; set; }
        public int SortIndex { get; set; }
        public bool IsActive { get; set; }
    }
}
