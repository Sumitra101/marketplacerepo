﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Plans
{
    public class PlanFeatureDetail
    {
        public Guid Id { get; set; }
        public Guid FeatureId { get; set; }
        public PlanFeature PlanFeature { get; set; }
        public Guid SubscriptionPlanId { get; set; }
        public SubscriptionPlan SubscriptionPlan { get; set; }
        public int PlanDetailValue { get; set; }
        public string Description { get; set; }
    }
}
