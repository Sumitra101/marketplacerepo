﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Plans
{
    public class PlanType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
