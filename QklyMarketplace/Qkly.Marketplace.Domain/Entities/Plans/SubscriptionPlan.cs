﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Plans
{
    public class SubscriptionPlan
    {
        public Guid Id { get; set; }
        public Guid CreatedById { get; set; }  //foreign key
        public Profile Profile { get; set; }
        public Guid PlanTypeId { get; set; } //foreign key
        public PlanType PlanType { get; set; }
        public string Name { get; set; }
        public decimal PricePerUser { get; set; }

    }
}
