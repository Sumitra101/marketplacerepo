﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using Qkly.Marketplace.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities.Plans
{
    public class ProfileSubscription
    {
        public Guid Id { get; set; }
        public Guid ProfileId { get; set; }  //foreign key
        public Profile Profile { get; set; }
        public Guid SubscriptionPlanId { get; set; }  //foreign key
        public SubscriptionPlan SubscriptionPlan { get; set; }
        public Guid PlanTypeId { get; set; }
        public PlanType PlanType { get; set; }
        public int NumberOfUsers { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime SignUpDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime RenewDate { get; set; }
        public ProfileStatus ProfileStatus { get; set; }
        public bool IsTrial { get; set; }
        public DateTime TrialStartDate { get; set; }

    }
}
