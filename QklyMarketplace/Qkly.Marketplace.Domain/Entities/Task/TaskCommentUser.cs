﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class TaskCommentUser
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; } // foreign key
        public Tasks Task { get; set; }
        public Guid CommentId { get; set; } // foreign key
        public TaskComment Comment { get; set; }
        public Guid CommenterProfileId { get; set; } // foreign key
        public Profile CommenterProfile { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeleted { get; set; }
    }
}