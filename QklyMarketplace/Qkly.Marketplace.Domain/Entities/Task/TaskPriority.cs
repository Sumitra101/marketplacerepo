﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class TaskPriority
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public int Level { get; set; }
    }
}