﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class TaskPercentage
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; } // foreign key
        public Tasks Task { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Message { get; set; }
        public bool IsDeleted { get; set; }
        public Guid CreatedById { get; set; } // foreign key
        public Profile CreatedBy { get; set; }
        public decimal CompletePercent { get; set; }
        public Guid ProjectId { get; set; } // foreign key
        public Project Project { get; set; }
        public decimal PreviousPercent { get; set; }
        public bool IsModified { get; set; }
        public Guid ModifiedById { get; set; } // foreign key
        public Profile ModifiedBy { get; set; }
    }
}