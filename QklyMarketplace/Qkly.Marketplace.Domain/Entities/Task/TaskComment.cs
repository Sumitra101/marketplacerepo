﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class TaskComment
    {
        public Guid Id { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid CommenterProfileId { get; set; } // foreign key
        public Profile CommenterProfile { get; set; }
        public Guid ParentThreadId { get; set; }
        public Guid TaskId { get; set; } // foreign key

        public Tasks Task { get; set; }
        public Guid IsDeleted { get; set; }
    }
}