﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class Tasks
    {
        public Guid Id { get; set; }
        public Guid AssignedUserId { get; set; } // foreign key

        public Profile AssignedUser { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public Guid ProjectId { get; set; } // foreign key
        public Project Project { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid StatusId { get; set; } // foreign key

        public Status Status { get; set; }
        public decimal CompletePercent { get; set; }

        public bool IsDeleted { get; set; }
        public Guid CreatedById { get; set; } // foreign key

        public Profile CreatedBy { get; set; }
        public decimal ProjectWeight { get; set; }

        public bool IsTernary { get; set; }
        public Guid PriorityId { get; set; } // foreign key

        public TaskPriority Priority { get; set; }
        public int Row { get; set; }

        public bool IsModified { get; set; }
        public Guid ModifiedById { get; set; } // foreign key
        public Profile ModifiedBy { get; set; }
    }
}