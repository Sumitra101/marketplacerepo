﻿using Qkly.Marketplace.Domain.Entities.Accounts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Qkly.Marketplace.Domain.Entities
{
    public class ProfileTask
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; } // foreign key
        public Tasks Task { get; set; }
        public Guid ProfileId { get; set; } // foreign key
        public Profile Profile { get; set; }
    }
}